Dear Brian P. Kemp,

In this trying time of US government overreach with mandates, it may be appropriate to have contingency plans in-place.

It's worth noting the US government seeks to try to sneak the OSHA mandate in the Congressional gravy train reconciliation bill (see: https://www.forbes.com/sites/adamandrzejewski/2021/09/28/bidens-vax-mandate-to-be-enforced-by-fining-companies-70000-to-700000/?sh=42ccdc6e1c0d).

Therefore, not only should Heads of State be seeking to litigate against the OSHA mandate, they should be proactively attempting to expose and resist efforts to sneak said mandate via the backdoor.

States should also consider amongst themselves as to whether to suspend all proceedings with OSHA and revoke any previously agreed OSHA plans in the event the US government somehow passes a mandate by the backdoor. States should seek to form a unified alliance with other like-minded States so they may coordinate efforts in such a way that increases impact.

I have copied a similar message to the Heads of other pro-freedom States (Alabama, Alaska, Arizona, Arkansas, Florida, Indiana, Kansas, Kentucky, Louisiana, Mississippi, Missouri, Montana, Nebraska, New Hampshire, North Dakota, Ohio, Oklahoma, South Carolina, South Dakota, Texas, Utah, West Virginia, and Wyoming) in the hopes you may be encouraged to join forces against this tyranny.

You may also be interested in the COVID-19 questions document that contains numerous peer-reviewed citations:
https://gitlab.com/TheUnderdog/general-research/-/tree/main/COVID-19-Shot-Questions/Revision-4-1

Kind Regards,

TheUnderdog
