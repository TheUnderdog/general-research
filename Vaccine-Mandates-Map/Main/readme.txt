Steps for re-creating the Vaccines Mandates Status map for editing purposes:

1. Download 'mapchartSave__world__.txt' from this subfolder 
2. Visit: https://mapchart.net/world.html
3. Go to "Save - Upload Map Configuration" found at the bottom-right
4. Under 'Upload', found in the middle of the prompt, click 'Choose a file...'
5. Select the 'mapchartSave__world__.txt' file you downloaded
6. Click the "Upload Map Configuration" button
7. Begin editing or updating the map
8. Once finished, go to "Save - Upload Map Configuration" found at the bottom-right
9. Click "Save Map Configuration" and save the file somewhere
